//
//  AppFont.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

enum FontType: String {
    case regular = "Raleway-Regular"
    case medium = "Raleway-Medium"
    case semi_bold = "Raleway-SemiBold"
    case bold = "Raleway-Bold"
}
