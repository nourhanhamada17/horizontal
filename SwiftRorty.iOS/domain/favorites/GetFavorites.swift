//
//  GetFavorites.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation
import Resolver

class GetFavorites {
    @Injected
    private var repository: FavoriteRepository
    
    func invoke() -> [CharacterDto] {
      return repository.getFavoriteList().map { (entity: FavoriteEntity) in
           entity.toCharacterDto()
       }
    }
}
