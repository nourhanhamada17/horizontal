//
//  DeleteFavorite.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation
import Resolver

class DeleteFavorite {
    @Injected
    private var repository: FavoriteRepository
    
    func invoke(id: Int) {
        repository.deleteFavorite(id: id)
    }
}
