//
//  GetCharacterDetail.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation
import Combine
import Resolver

class GetCharacterDetail {
    @Injected private var repository: CharacterRepository
    
    func invoke(characterId: Int) -> AnyPublisher<CharacterDto, Error> {
        repository.getCharacter(characterId: characterId).map { (response: CharacterInfo) in
            response.toCharacterDto()
        }.eraseToAnyPublisher()
    }
}
