//
//  Endpoint.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

protocol Endpoint {
    var url: URL { get }
    var path: String { get }
}
