//
//  RestClientErrors.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

//swiftlint:disable:this
enum RestClientErrors: Error {
    case requestFailed(error: Error)
    case requestFailed(code: Int)
    case noDataReceived
    case jsonDecode(error: Error)
}
