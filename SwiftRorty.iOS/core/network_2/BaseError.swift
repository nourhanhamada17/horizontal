//
//  BaseError.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

enum BaseError : Error {
    case parse(description: String)
    case api(description: String)
}
