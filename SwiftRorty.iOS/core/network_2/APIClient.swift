//
//  APIClient.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation
import Combine

protocol APIClient {
    func request<T: Decodable>(_ urlRequest: URLRequest) -> AnyPublisher<T, BaseError>
}
