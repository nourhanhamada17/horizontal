//
//  LocationResponse.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

struct LocationResponse: Codable {
    let pageInfo: PageInfo?
    let location: [LocationInfo]?
    
    enum CodingKeys: String, CodingKey {
        case pageInfo = "pageInfo"
        case location = "results"
    }
}
