//
//  CharacterResponse.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

struct CharacterResponse: Codable {
    let pageInfo: PageInfo
    let results: [CharacterInfo]
    
    enum CodingKeys: String, CodingKey {
        case pageInfo = "info"
        case results = "results"
    }
}
