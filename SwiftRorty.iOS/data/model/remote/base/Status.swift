//
//  Status.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

enum Status: String, Codable {
    case alive = "Alive"
    case dead = "Dead"
    case unknown = "unknown"
}
