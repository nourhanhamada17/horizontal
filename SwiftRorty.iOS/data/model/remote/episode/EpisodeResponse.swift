//
//  EpisodeResponse.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

struct EpisodeResponse: Codable {
    let pageInfo: PageInfo?
    let results: [EpisodeInfo]?
}
