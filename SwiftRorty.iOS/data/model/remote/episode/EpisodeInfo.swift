//
//  EpisodeInfo.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

struct EpisodeInfo: Codable {
    let airDate: String?
    let characters: [String]?
    let created: String?
    let episode: String?
    let id: Int?
    let name: String?
    let url: String?
}
