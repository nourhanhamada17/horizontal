//
//  CharacterListDto.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

struct CharacterListDto {
    let info: PageInfo
    let characters: [CharacterDto]
}
