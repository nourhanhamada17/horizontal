//
//  LocationDto.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

struct LocationDto {
    let locationId: Int?
    let name: String?
    let url: String?
}
