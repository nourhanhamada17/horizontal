//
//  KeyValueModel.swift
//  SwiftRorty.iOS
//
//  Created by nourhan hamada on 08/12/2022.
//

import Foundation

struct KeyValueModel: Identifiable {
    let id: Int?
    let key: String?
    let value: String?
}
